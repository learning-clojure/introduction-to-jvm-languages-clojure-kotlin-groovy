package org.elu.kotlin.learning.javafx.demo

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.chart.LineChart
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart
import javafx.scene.control.CheckBox
import javafx.scene.control.Control
import javafx.scene.control.Label
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.Priority
import javafx.stage.Stage
import org.elu.kotlin.learning.javafx.demo.extensions.prettyPrint

class KotlinJavaFXDemo: Application() {
    private val chart: LineChart<Number, Number>
    private val checkBoxSin = CheckBox("sin(x)")
    private val checkBoxCos = CheckBox("cos(x)")
    private val checkBoxSinMin = CheckBox("sin(x) * -1")
    private val checkBoxCosMin = CheckBox("cos(x) * -1")

    init {
        val axisX = NumberAxis()
        axisX.label = "X"

        val axisY = NumberAxis()
        axisY.label = "Y"

        chart = LineChart<Number, Number>(axisX, axisY)
    }

    override fun start(primaryStage: Stage) {
        primaryStage.title = "Kotlin JavaFX Demo"

        val mainPane = BorderPane()
        mainPane.top = createHeaderPane(primaryStage.title)
        mainPane.center = createChartPane()
        mainPane.bottom = createInputPane()

        val scene = Scene(mainPane, 500.0, 500.0)
        primaryStage.scene = scene

        primaryStage.show()
    }

    private fun createInputPane(): Pane {
        val inputPane = HBox()
        val labeVariations = Label("Variations: ")

        val controls = listOf(checkBoxSin, checkBoxCos, labeVariations, checkBoxSinMin, checkBoxCosMin)
        for (c: Control in controls) {
            c.minWidth = 80.0
            inputPane.children.add(c)

            if (c is CheckBox) {
                c.onAction = object: EventHandler<ActionEvent> {
                    override fun handle(event: ActionEvent?) {
                        drawChart()
                    }
                }
            }
        }
        checkBoxSinMin.disableProperty().bind(checkBoxSin.selectedProperty().not())
        checkBoxCosMin.disableProperty().bind(checkBoxCos.selectedProperty().not())
        return inputPane
    }

    private fun createChartPane(): Pane {
        val pane = Pane()

        pane.children.add(chart)
        HBox.setHgrow(chart, Priority.ALWAYS)
        return pane
    }

    private fun createHeaderPane(title: String): Pane {
        val pane = Pane()
        pane.prettyPrint(30.0, title)
        return pane
    }

    fun drawChart() {
        chart.data.clear()

        if (checkBoxSin.isSelected) {
            val seriesSin = XYChart.Series<Number, Number>()
            seriesSin.name = if (checkBoxSinMin.isSelected) checkBoxSinMin.text else checkBoxSin.text

            val multiplierSin = if (checkBoxSinMin.isSelected) -1 else 1
            fillData(seriesSin, fun(x) = Math.sin(x) * multiplierSin)
            chart.data.add(seriesSin)
        }

        if (checkBoxCos.isSelected) {
            val seriesSin = XYChart.Series<Number, Number>()
            seriesSin.name = if (checkBoxCosMin.isSelected) checkBoxCosMin.text else checkBoxCos.text

            val multiplierSin = if (checkBoxCosMin.isSelected) -1 else 1
            fillData(seriesSin, fun(x) = Math.cos(x) * multiplierSin)
            chart.data.add(seriesSin)
        }
    }

    private fun fillData(series: XYChart.Series<Number, Number>, f: (Double) -> Double) {
        for (x in 0..360) {
            val radianX = Math.toRadians(x.toDouble())
            val y = f(radianX)
            series.data.add(XYChart.Data<Number, Number>(x, y))
        }
    }
}

fun main(args: Array<String>) {
    Application.launch(KotlinJavaFXDemo::class.java)
}
